#include "registr.h"
#include "ui_registr.h"
#include <QSqlDatabase>
#include <QSqlQuery>




registr::registr(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::registr)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

}


registr::~registr()
{
    delete ui;
}

void registr::on_pushButton_clicked()
{
    QSqlQuery query;
    query.prepare("INSERT INTO User (login, password) VALUES(?, ?)");
    query.addBindValue(ui->LoginPass->text());
    query.addBindValue(ui->PasswordPass->text());
    query.exec();
    close();
}


