#include "admin.h"
#include "ui_admin.h"
#include "QMessageBox"
#include <QSqlDatabase>
#include "QTimer"
#include <QSqlQuery>


admin::admin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::admin)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowSystemMenuHint);

    AddBdDta();

}

admin::~admin()
{
    delete ui;
}


void admin::on_Exit_clicked(){
    auto config = QMessageBox::question(this, "Завершение программы", "Вы действиетельно хотите выйти?",
                                       "Да","Нет",NULL,1);
    if(config != 1 ){
        close();
        return;
    }
}




void admin::AddBdDta(){
    db = new DbConnect;
    db->CreateDb();

   model = new QSqlTableModel(this);
   model->setTable("Tele");
   if (model->select()){
       ui->TableOut->setModel(model);
       ui->TableOut->resizeRowsToContents();
       ui->TableOut->setColumnHidden(0,true);
       ui->TableOut->resizeColumnsToContents();
       ui->TableOut->horizontalHeader()->setStretchLastSection(true);
       model->setHeaderData(1,Qt::Horizontal,tr("Название"));
       model->setHeaderData(3,Qt::Horizontal,tr("Жанр"));
       model->setHeaderData(2,Qt::Horizontal,tr("Время"));
       model->setHeaderData(4,Qt::Horizontal,tr("Телеканал"));
       model->setHeaderData(5,Qt::Horizontal,tr("День недели"));
       ui->TableOut->setFocus();

   }
}
void admin::slotUpdateModels()
{
    model->select();
}

void admin::on_addBtn_clicked(){
    addBtn = new AddTeleroram();
    if (!connect(ui->addBtn, SIGNAL(clicked()),this,SLOT(on_addBtn_clicked()))){
         qFatal("Слот не работает addBtn");
    }
    connect(addBtn, SIGNAL(signalReady()), this, SLOT(slotUpdateModels()));
    addBtn ->show();

}


void admin::on_updateBtn_clicked(){
    model->submitAll();
}

void admin::on_editBtn_clicked(){
 //model->updateRowInTable(ui->TableOut->currentIndex().row());
 editBtn = new EditTeleprogram();
 if (!connect(ui->editBtn, SIGNAL(clicked()),this,SLOT(on_editBtn_clicked()))){
      qFatal("Слот не работает addBtn");
 }
 connect(editBtn, SIGNAL(signalReady()), this, SLOT(slotUpdateModels()));
 editBtn ->show();

}

void admin::on_deleteBtn_clicked()
{
    model->removeRow(ui->TableOut->currentIndex().row());
  //  connect(ui->deleteBtn, SIGNAL(clicked()),this,SLOT(on_deleteBtn_clicked()));

}
