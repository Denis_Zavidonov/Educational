#ifndef EDITTELEPROGRAM_H
#define EDITTELEPROGRAM_H

#include <QWidget>
#include <QDataWidgetMapper>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
namespace Ui {
class EditTeleprogram;
}

class EditTeleprogram : public QWidget
{
    Q_OBJECT

public:
    explicit EditTeleprogram(QWidget *parent = 0);
    ~EditTeleprogram();
signals:
    void signalReady();
private slots:

    void on_UpbdateBtn_clicked();

    void on_pushButton_clicked();

private:
    QSqlTableModel *model;
    Ui::EditTeleprogram *ui;
};

#endif // EDITTELEPROGRAM_H
