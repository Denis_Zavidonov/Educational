#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include "admin.h"
#include "registr.h"
#include "mainwindow.h"

namespace Ui {
class login;
}

class login : public QWidget
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = 0);
    ~login();


private slots:
    void on_SendBtn_clicked();


    void on_pushButton_2_clicked();

    //MainWindow *sWindow;


private:

    admin *window;
    registr *wind;
    MainWindow *sWindow;

    Ui::login *ui;
};

#endif // LOGIN_H
