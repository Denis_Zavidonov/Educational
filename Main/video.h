#ifndef VIDEO_H
#define VIDEO_H
#include <QMediaPlayer>
#include <QFileDialog>
#include <QVideoWidget>

#include <QWidget>

namespace Ui {
class Video;
}

class Video : public QWidget
{
    Q_OBJECT

public:
    explicit Video(QWidget *parent = 0);
    ~Video();
    QString qsDataPath;
    QMediaPlayer *player;
    QVideoWidget *videoWidget;

private slots:
    void on_btn_add_clicked();

private:
    Ui::Video *ui;
};

#endif // VIDEO_H
