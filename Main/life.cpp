#include "life.h"
#include "ui_life.h"
#include "QMessageBox"
#include <QVideoWidget>
#include <QMediaPlayer>
#include <QFileDialog>
#include <QDir>


life::life(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::life)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowSystemMenuHint);



}

life::~life()
{
    delete ui;
}

void life::on_Exit_clicked()
{
    auto config = QMessageBox::question(this, "Завершение программы", "Вы действиетельно хотите выйти?",
                                              "Да","Нет",NULL,1);
      if(config != 1 ){
         close();
         return;
      }
}

void life::on_Close_clicked(){
    QStringList files = QFileDialog::getOpenFileNames(this,
                                                      tr("Open video files"),
                                                      QString(),
                                                      tr("Media files  (*.vob *.mpg "
                                                         "*.mpeg *.mp4 *.wmv *.avi)"));

}
