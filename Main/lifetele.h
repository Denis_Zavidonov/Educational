#ifndef LIFETELE_H
#define LIFETELE_H

#include <QWidget>

namespace Ui {
class LifeTele;
}

class LifeTele : public QWidget
{
    Q_OBJECT

public:
    explicit LifeTele(QWidget *parent = 0);
    ~LifeTele();

private:
    Ui::LifeTele *ui;
};

#endif // LIFETELE_H
