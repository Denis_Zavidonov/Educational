#-------------------------------------------------
#
# Project created by QtCreator 2018-04-11T20:05:44
#
#-------------------------------------------------

QT       += core gui
QT       += printsupport
QT       += multimedia multimediawidgets
QT       += sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Main
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        login.cpp \
        registr.cpp \
        admin.cpp \
    dbconnect.cpp \
    video.cpp \
    addteleroram.cpp \
    editteleprogram.cpp

HEADERS += \
        login.h \
    mainwindow.h \
    registr.h \
    admin.h \
    dbconnect.h \
    video.h \
    addteleroram.h \
    editteleprogram.h

FORMS += \
        mainwindow.ui \
    login.ui \
    registr.ui \
    admin.ui \
    video.ui \
    addteleroram.ui \
    editteleprogram.ui

RESOURCES += \
    files.qrc

DISTFILES += \
    ../Icon/fotolia_facebook_like_61802123_xs1_sbp.jpg \
    ../Icon/main-thumb-t-1800-200-lOn8kKfhqfcTcKdt2GwaLfGnC0jEjHmV.jpeg \
    ../Icon/xrichiedi_certificato.jpg.pagespeed.ic.Q61xcI3OmB.jpg \
    ../Icon/45.png
