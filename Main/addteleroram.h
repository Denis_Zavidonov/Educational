#ifndef ADDTELERORAM_H
#define ADDTELERORAM_H

#include <QWidget>
#include "dbconnect.h"
#include "QMessageBox"
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQuery>
namespace Ui {
class AddTeleroram;
}

class AddTeleroram : public QWidget
{
    Q_OBJECT

public:
    explicit AddTeleroram(QWidget *parent = 0);
    ~AddTeleroram();
    void on_FailData_linkActivated();
    //void DataBaseTv();

signals:
    void signalReady();

private slots:

    void on_AddButton_clicked();
    void on_pushButton_clicked();

private:
    QSqlTableModel *model;
    Ui::AddTeleroram *ui;
};

#endif // ADDTELERORAM_H
