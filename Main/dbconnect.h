#ifndef DBCONNECT_H
#define DBCONNECT_H
#include <QSqlDatabase>
#define DBNAME "Teleprogramma.db"
class DbConnect
{
public:
    DbConnect();
    void CreateDb();
    void CreateTeleprogram();
    void CreateUser();
    QSqlDatabase dataDB;
};

#endif // DBCONNECT_H
