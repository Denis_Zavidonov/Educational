#include "editteleprogram.h"
#include "ui_editteleprogram.h"

EditTeleprogram::EditTeleprogram(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditTeleprogram)
{
    ui->setupUi(this);
}

EditTeleprogram::~EditTeleprogram()
{
    delete ui;
}



void EditTeleprogram::on_UpbdateBtn_clicked()
{
    if (!connect(ui->UpbdateBtn, SIGNAL(clicked()),this,SLOT(on_UpbdateBtn_clicked()))){
         qFatal("Слот не работает wind");
    }
    QSqlQuery query;
    query.prepare("Update Tele SET name=:names, genre=:genres,teleprogramma=:teleprogrammas,day=:days,time=:times WHERE id)");
    query.bindValue(":names",ui->Title->text());
    query.bindValue(":genres",ui->genre->text());
    query.bindValue(":teleprogrammas",ui->TvChannel->text());
    query.bindValue(":days",ui->day->text());
    query.bindValue(":times",ui->Time->text());
    query.exec();
}

void EditTeleprogram::on_pushButton_clicked()
{
    emit signalReady();
    this->close();
}
