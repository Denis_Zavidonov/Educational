#include "addteleroram.h"
#include "ui_addteleroram.h"

AddTeleroram::AddTeleroram(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddTeleroram)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowSystemMenuHint);
}

AddTeleroram::~AddTeleroram()
{
    delete ui;
}

void AddTeleroram::on_AddButton_clicked()
{
    if (!connect(ui->AddButton, SIGNAL(clicked()),this,SLOT(on_AddButton_clicked()))){
         qFatal("Слот не работает wind");
    }
    QSqlQuery query;
    query.prepare("INSERT INTO Tele (name, genre,teleprogramma,day,time) VALUES(?,?,?,?,?)");
    query.addBindValue(ui->Title->text());
    query.addBindValue(ui->genre->text());
    query.addBindValue(ui->TvChannel->text());
    query.addBindValue(ui->day->text());
    query.addBindValue(ui->Time->text());
    query.exec();
}
//
void AddTeleroram::on_pushButton_clicked()
{
    emit signalReady();
    this->close();
}
