#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include "video.h"
#include "dbconnect.h"
#include <QSqlTableModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    //void AddIcon();
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void AddBdDta();

private slots:

    void on_actionContext_Help_triggered();
    void on_actionPrint_2_triggered();
    void on_actionCheck_Update_triggered();
    void on_Exit_clicked();
    void updateDateHour();


    void on_Video_clicked();


    void on_PN_clicked();

    void on_VT_clicked();

    void on_CR_clicked();

    void on_CH_clicked();

    void on_PT_clicked();

    void on_SB_clicked();

    void on_VS_clicked();

    void on_Music_clicked();

    void on_Sport_clicked();

    void on_two_clicked();

    void on_oneChanel_clicked();

    void on_actionTeleprogramma_triggered();

private:

    DbConnect *db;
    QSqlTableModel *model;
    Video *openVid;

    Ui::MainWindow *ui;


};


#endif // MAINWINDOW_H
