#include "video.h"
#include "ui_video.h"
#include <QMediaPlayer>
#include <QFileDialog>

Video::Video(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Video)
{
    ui->setupUi(this);


     videoWidget = new QVideoWidget(this);
     player = new QMediaPlayer(this);
     player->setVideoOutput(videoWidget);
     ui->verticalLayout->addWidget(videoWidget);

     connect(ui->btn_play, &QPushButton::clicked, player, &QMediaPlayer::play);
     connect(ui->btn_stop, &QPushButton::clicked, player, &QMediaPlayer::stop);

}

Video::~Video()
{
    delete ui;
}

void Video::on_btn_add_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open music and video files"),
     qsDataPath, tr( "Media files (*.vob *.mpg *.mpeg *.mp4 *.wmv *.avi) " ) );
      if (!fileName.isEmpty()) {
           player->setMedia(QUrl::fromLocalFile(fileName));
      }
      qsDataPath = QFileInfo(fileName).path();
}
