#ifndef LIFE_H
#define LIFE_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QVideoWidget>

namespace Ui {
class life;
}

class life : public QMainWindow
{
    Q_OBJECT

public:
    explicit life(QWidget *parent = 0);
    ~life();


private slots:
    void on_Exit_clicked();
;
    void on_Close_clicked();

signals:
     void lifeChanel();
private:
   QMediaPlayer *player;
   QVideoWidget *videoWidget;
    Ui::life *ui;
};

#endif // LIFE_H
