#ifndef ADMIN_H
#define ADMIN_H

#include <QWidget>
#include "dbconnect.h"
#include <QSqlTableModel>
#include <addteleroram.h>
#include "editteleprogram.h"


namespace Ui {
class admin;
}

class admin : public QWidget
{
    Q_OBJECT

public:
    explicit admin(QWidget *parent = 0);
    ~admin();
    void AddBdDta();


private slots:
    void on_Exit_clicked();

    void slotUpdateModels();
    void on_addBtn_clicked();

    void on_updateBtn_clicked();

    void on_editBtn_clicked();

    void on_deleteBtn_clicked();

private:
    AddTeleroram *addBtn;
    EditTeleprogram *editBtn;
    DbConnect *db;
    QSqlTableModel *model;
    Ui::admin *ui;
};

#endif // ADMIN_H
