#include "login.h"
#include "ui_login.h"
#include "QMessageBox"

login::login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::login)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

    sWindow = new MainWindow();
    window = new admin();
    wind = new registr();
}

login::~login()
{
    delete ui;
    delete sWindow;
    delete window;
    delete wind;
}

void login::on_SendBtn_clicked(){
    auto bt = ui->SendBtn;
    //auto pass = ui->passwordPas;
    QString logPas = ui->loginPass->text();
    QString pass = ui->passwordPas->text();
    //pass->setEchoMode();

    if (logPas == "admin" && pass == "1") {

        if (!connect(bt, SIGNAL(clicked()), this, SLOT(on_SendBtn_clicked()))){
            qFatal("Слот не работает window");
        }
        window->show();
        this ->close();

    } else if (logPas == "root" && pass == "root" ) {

        if (!connect(bt, SIGNAL(clicked()), this, SLOT(on_SendBtn_clicked()))){
          qFatal("Слот не работает sWindow");
        }
        sWindow->show();
        this ->close();

    }else {
        QMessageBox::warning(this,"Ошибка","Такого пользователя нет!");
    }

}

void login::on_pushButton_2_clicked()
{

    if (!connect(ui->pushButton_2, SIGNAL(clicked()),this,SLOT(on_pushButton_2_clicked()))){
         qFatal("Слот не работает wind");
    }
    wind ->show();
    this ->close();
}
