#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QMessageBox"
#include "QDate"
#include "QtPrintSupport"
#include "QDialog"
#include <QSqlTableModel>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

   this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowSystemMenuHint);
    AddBdDta();
    QTimer* time = new QTimer(this);
    if(!connect(time ,SIGNAL(timeout()),SLOT(updateDateHour()))){
        qFatal("Сигнал time не работает");
   }
   time->start(500);

}
MainWindow::~MainWindow() {
    delete ui;
}
void MainWindow::updateDateHour(){
    ui->FailMinute->display(QTime::currentTime().minute());
    ui->FailHour->display(QTime::currentTime().hour());
    ui->FailDate->setText(QDate::currentDate().toString(Qt::SystemLocaleLongDate));
}
void MainWindow::on_actionContext_Help_triggered() {
    QMessageBox *msq = new QMessageBox;
    msq ->setText("Справка");
    msq ->exec();
    delete msq;
}

void MainWindow::on_actionPrint_2_triggered() {
    QPrinter printer;
    QPrintDialog* printDial = new QPrintDialog(&printer);
    if (printDial->exec() == QDialog::Accepted ){
        //Печать
    }
    delete printDial;
}

void MainWindow::on_actionCheck_Update_triggered() {
    QMessageBox *msq = new QMessageBox;
    msq ->setText("Обновлений нет");
    msq ->exec();
    delete msq;
}

void MainWindow::on_Exit_clicked() {
 auto config = QMessageBox::question(this, "Завершение программы", "Вы действиетельно хотите выйти?",
                                           "Да","Нет",NULL,1);
   if(config != 1 ){
      close();
      return;
   }
}

void MainWindow::AddBdDta(){
    db = new DbConnect;
    db->CreateDb();

   model = new QSqlTableModel(this);
   model->setTable("Tele");
   if (model->select()){
       ui->TableOut->setModel(model);
       ui->TableOut->resizeRowsToContents();
       ui->TableOut->setColumnHidden(0,true);
       ui->TableOut->resizeColumnsToContents();
       ui->TableOut->setEditTriggers(QAbstractItemView::NoEditTriggers);
       ui->TableOut->horizontalHeader()->setStretchLastSection(true);
       model->setHeaderData(1,Qt::Horizontal,tr("Название"));
       model->setHeaderData(3,Qt::Horizontal,tr("Жанр"));
       model->setHeaderData(2,Qt::Horizontal,tr("Время"));
       model->setHeaderData(4,Qt::Horizontal,tr("Телеканал"));
       model->setHeaderData(5,Qt::Horizontal,tr("День недели"));
       ui->TableOut->setFocus();
   }

}

void MainWindow::on_Video_clicked() {
    openVid = new Video();
    if (!connect(ui->Video, SIGNAL(clicked()),this,SLOT(on_Video_clicked()))){
         qFatal("Слот не работает openVideo");
    }
    openVid->show();
    this->close();
}


void MainWindow::on_PN_clicked()
{
    if (!connect(ui->PN, SIGNAL(clicked()),this,SLOT(on_PN_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Понедельник'");
    model->select();
}

void MainWindow::on_VT_clicked()
{
    if (!connect(ui->VT, SIGNAL(clicked()),this,SLOT(on_VT_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Вторник'");
    model->select();
}

void MainWindow::on_CR_clicked()
{
    if (!connect(ui->CR, SIGNAL(clicked()),this,SLOT(on_CR_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Среда'");
    model->select();
}

void MainWindow::on_CH_clicked()
{
    if (!connect(ui->CH, SIGNAL(clicked()),this,SLOT(on_CH_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Четверг'");
    model->select();
}

void MainWindow::on_PT_clicked()
{
    if (!connect(ui->PT, SIGNAL(clicked()),this,SLOT(on_PT_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Пятница'");
    model->select();
}

void MainWindow::on_SB_clicked()
{
    if (!connect(ui->SB, SIGNAL(clicked()),this,SLOT(on_SB_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Суббота'");
    model->select();
}

void MainWindow::on_VS_clicked()
{
    if (!connect(ui->VS, SIGNAL(clicked()),this,SLOT(on_VS_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("day='Воскресенье'");
    model->select();
}

void MainWindow::on_Music_clicked()
{
    if (!connect(ui->Music, SIGNAL(clicked()),this,SLOT(on_Music_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("teleprogramma='Муз ТВ'");
    model->select();
}

void MainWindow::on_Sport_clicked()
{
    if (!connect(ui->Sport, SIGNAL(clicked()),this,SLOT(on_Sport_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("teleprogramma='Спорт ТВ'");
    //model->setFilter("between day='Понедельник' and day='Воскресенье'");
    model->select();
}
//and day='Воскресенье' and day='Понедельник' and day='Вторник' and day='Пятница'
void MainWindow::on_two_clicked()
{
    if (!connect(ui->two, SIGNAL(clicked()),this,SLOT(on_two_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("teleprogramma='Netflix'");
    model->select();
}

void MainWindow::on_oneChanel_clicked()
{
    if (!connect(ui->oneChanel, SIGNAL(clicked()),this,SLOT(on_oneChanel_clicked()))){
         qFatal("Слот не работает on_PN_clicked");
    }
    model->setFilter("teleprogramma='Первый канал'");
    model->select();
}

void MainWindow::on_actionTeleprogramma_triggered()
{
    QMessageBox *msq = new QMessageBox;
    msq ->setText("Разработчик Завидонов Денис гр.814321");
    msq ->exec();
    delete msq;
}
