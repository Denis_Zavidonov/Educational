#include "dbconnect.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include "QFile"

DbConnect::DbConnect(){

}

void DbConnect::CreateDb(){
    dataDB = QSqlDatabase::addDatabase("QSQLITE");
    dataDB.setDatabaseName("Teleprogramma.db");
    if( !dataDB.open() ) {
           qFatal("База данных Teleprogramma не создана");
   }
    CreateTeleprogram();
    CreateUser();

}
void DbConnect::CreateTeleprogram(){
    QSqlQuery query;
        query.exec(
                    "CREATE TABLE Tele("
                                    "   id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    "   name VARCHAR( 80 ) NOT NULL, "
                                    "   time VARCHAR( 40 ) NOT NULL , "
                                    "   genre VARCHAR( 40 ) NOT NULL, "
                                    "   teleprogramma VARCHAR( 40 ) NOT NULL, "
                                    "   day VARCHAR( 40 ) NOT NULL "
                                    ")"
        );
}

void DbConnect::CreateUser(){
    QSqlQuery query;
        query.exec(
                    "CREATE TABLE User("
                                    "   id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    "   login VARCHAR( 20 ) NOT NULL,"
                                    "   password VARCHAR( 20 ) NOT NULL"
                                    ")"
        );
}

